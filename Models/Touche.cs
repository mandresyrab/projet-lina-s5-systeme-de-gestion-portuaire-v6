﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("TOUCHE")]
    public partial class Touche
    {
        int idQuai;
        [Key]
        [Column("IDTOUCHE")]
        public int Idtouche { get; set; }
        [Column("IDPREVISION")]
        public int Idprevision { get; set; }
        [Column("IDQUAI")]
        public int Idquai {
            get { return idQuai; }
            set
            {
                Quai quai = Quai.FindById(value);
                Prevision prev = Prevision.FindById(this.Idprevision);
                Navire nav = Navire.FindById(prev.Idnavire);
                if (quai.Profondeur < nav.Profondeur) throw new Exception("Quai Incompatible");
                if (prev.SurQuai(this.Idtouche)) throw new Exception("Le Navire est encore occupé sur un quai");
                
                idQuai = value;
            }
        }
        [Column("DATE", TypeName = "datetime")]
        public DateTime Date { get; set; }

        [ForeignKey(nameof(Idprevision))]
        [InverseProperty(nameof(Prevision.Touches))]
        public virtual Prevision IdprevisionNavigation { get; set; }
        [ForeignKey(nameof(Idquai))]
        [InverseProperty(nameof(Quai.Touches))]
        public virtual Quai IdquaiNavigation { get; set; }
    }
}
