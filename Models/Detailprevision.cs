﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("DETAILPREVISION")]
    public partial class Detailprevision
    {
        int idQuai;
        [Key]
        [Column("IDDETAILPREVISION")]
        public int Iddetailprevision { get; set; }
        [Column("IDPREVISION")]
        public int Idprevision { get; set; }
        [Column("IDQUAI")]
        public int Idquai {
            get { return idQuai; }
            set
            {
                Quai quai = Quai.FindById(value);
                Navire nav = Navire.FindById(Prevision.FindById(Idprevision).Idnavire);
                if (quai.Profondeur < nav.Profondeur) throw new Exception("Quai incompatible");
                idQuai = value;
            }
        }
        [Column("ARRIVE", TypeName = "datetime")]
        public DateTime Arrive { get; set; }
        [Column("DEPART", TypeName = "datetime")]
        public DateTime Depart { get; set; }

        [ForeignKey(nameof(Idprevision))]
        [InverseProperty(nameof(Prevision.Detailprevisions))]
        public virtual Prevision IdprevisionNavigation { get; set; }
        [ForeignKey(nameof(Idquai))]
        [InverseProperty(nameof(Quai.Detailprevisions))]
        public virtual Quai IdquaiNavigation { get; set; }
    }
}
