﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    public partial class DBContext : DbContext
    {
        public DBContext()
        {
        }

        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Coursdevise> Coursdevises { get; set; }
        public virtual DbSet<Depart> Departs { get; set; }
        public virtual DbSet<Detailplafondpenalite> Detailplafondpenalites { get; set; }
        public virtual DbSet<Detailprevision> Detailprevisions { get; set; }
        public virtual DbSet<Devise> Devises { get; set; }
        public virtual DbSet<Horsplafond> Horsplafonds { get; set; }
        public virtual DbSet<Listeplafond> Listeplafonds { get; set; }
        public virtual DbSet<Navire> Navires { get; set; }
        public virtual DbSet<Penalitehorsplafond> Penalitehorsplafonds { get; set; }
        public virtual DbSet<Plafond> Plafonds { get; set; }
        public virtual DbSet<Plafondpenalite> Plafondpenalites { get; set; }
        public virtual DbSet<Prevision> Previsions { get; set; }
        public virtual DbSet<Quai> Quais { get; set; }
        public virtual DbSet<Tarrification> Tarrifications { get; set; }
        public virtual DbSet<Touche> Touches { get; set; }
        public virtual DbSet<Tranchepenalite> Tranchepenalites { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=port;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "French_CI_AS");

            modelBuilder.Entity<Coursdevise>(entity =>
            {
                entity.HasKey(e => e.Idcoursdevise)
                    .HasName("PK__COURSDEV__9896AC6881CEBD49");

                entity.Property(e => e.Idcoursdevise).ValueGeneratedNever();

                entity.HasOne(d => d.IddeviseNavigation)
                    .WithMany(p => p.Coursdevises)
                    .HasForeignKey(d => d.Iddevise)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__COURSDEVI__IDDEV__5BE2A6F2");
            });

            modelBuilder.Entity<Depart>(entity =>
            {
                entity.HasKey(e => e.Iddepart)
                    .HasName("PK__DEPART__AF2770BDD9DEAC8E");

                entity.Property(e => e.Iddepart).ValueGeneratedNever();

                entity.HasOne(d => d.IdprevisionNavigation)
                    .WithMany(p => p.Departs)
                    .HasForeignKey(d => d.Idprevision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DEPART__IDPREVIS__619B8048");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Departs)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DEPART__IDQUAI__628FA481");
            });

            modelBuilder.Entity<Detailplafondpenalite>(entity =>
            {
                entity.HasKey(e => e.Iddetailplafondpenalite)
                    .HasName("PK__DETAILPL__76602097D16DD6F9");

                entity.Property(e => e.Iddetailplafondpenalite).ValueGeneratedNever();

                entity.HasOne(d => d.IdplafondpenaliteNavigation)
                    .WithMany(p => p.Detailplafondpenalites)
                    .HasForeignKey(d => d.Idplafondpenalite)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DETAILPLA__IDPLA__6E01572D");
            });

            modelBuilder.Entity<Detailprevision>(entity =>
            {
                entity.HasKey(e => e.Iddetailprevision)
                    .HasName("PK__DETAILPR__AE7A7DC82F6F6310");

                entity.Property(e => e.Iddetailprevision).ValueGeneratedNever();

                entity.HasOne(d => d.IdprevisionNavigation)
                    .WithMany(p => p.Detailprevisions)
                    .HasForeignKey(d => d.Idprevision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DETAILPRE__IDPRE__5DCAEF64");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Detailprevisions)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DETAILPRE__IDQUA__5EBF139D");
            });

            modelBuilder.Entity<Devise>(entity =>
            {
                entity.HasKey(e => e.Iddevise)
                    .HasName("PK__DEVISE__E727873DB70560DE");

                entity.Property(e => e.Iddevise).ValueGeneratedNever();

                entity.Property(e => e.Nomdevise).IsUnicode(false);
            });

            modelBuilder.Entity<Horsplafond>(entity =>
            {
                entity.HasKey(e => e.Idhorsplafond)
                    .HasName("PK__HORSPLAF__05057130FE7EA78E");

                entity.Property(e => e.Idhorsplafond).ValueGeneratedNever();

                entity.HasOne(d => d.IddeviseNavigation)
                    .WithMany(p => p.Horsplafonds)
                    .HasForeignKey(d => d.Iddevise)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HORSPLAFO__IDDEV__693CA210");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Horsplafonds)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HORSPLAFO__IDQUA__68487DD7");
            });

            modelBuilder.Entity<Listeplafond>(entity =>
            {
                entity.HasKey(e => e.Idlisteplafond)
                    .HasName("PK__LISTEPLA__48D4F840DC8F63C2");

                entity.Property(e => e.Idlisteplafond).ValueGeneratedNever();

                entity.HasOne(d => d.IdplafondNavigation)
                    .WithMany(p => p.Listeplafonds)
                    .HasForeignKey(d => d.Idplafond)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LISTEPLAF__IDPLA__6754599E");
            });

            modelBuilder.Entity<Navire>(entity =>
            {
                entity.HasKey(e => e.Idnavire)
                    .HasName("PK__NAVIRE__9DDE6365BB81E31A");

                entity.Property(e => e.Idnavire).ValueGeneratedNever();

                entity.Property(e => e.Nomnavire).IsUnicode(false);
            });

            modelBuilder.Entity<Penalitehorsplafond>(entity =>
            {
                entity.HasKey(e => e.Idpenalitehorsplafond)
                    .HasName("PK__PENALITE__7497A1F253DD5660");

                entity.Property(e => e.Idpenalitehorsplafond).ValueGeneratedNever();

                entity.HasOne(d => d.IddeviseNavigation)
                    .WithMany(p => p.Penalitehorsplafonds)
                    .HasForeignKey(d => d.Iddevise)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PENALITEH__IDDEV__6EF57B66");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Penalitehorsplafonds)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PENALITEH__IDQUA__6FE99F9F");
            });

            modelBuilder.Entity<Plafond>(entity =>
            {
                entity.HasKey(e => e.Idplafond)
                    .HasName("PK__PLAFOND__058EF3E396CF376B");

                entity.Property(e => e.Idplafond).ValueGeneratedNever();

                entity.HasOne(d => d.IddeviseNavigation)
                    .WithMany(p => p.Plafonds)
                    .HasForeignKey(d => d.Iddevise)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PLAFOND__IDDEVIS__66603565");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Plafonds)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PLAFOND__IDQUAI__656C112C");
            });

            modelBuilder.Entity<Plafondpenalite>(entity =>
            {
                entity.HasKey(e => e.Idplafondpenalite)
                    .HasName("PK__PLAFONDP__7757E341E21C1837");

                entity.Property(e => e.Idplafondpenalite).ValueGeneratedNever();

                entity.HasOne(d => d.IddeviseNavigation)
                    .WithMany(p => p.Plafondpenalites)
                    .HasForeignKey(d => d.Iddevise)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PLAFONDPE__IDDEV__6D0D32F4");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Plafondpenalites)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PLAFONDPE__IDQUA__6C190EBB");
            });

            modelBuilder.Entity<Prevision>(entity =>
            {
                entity.HasKey(e => e.Idprevision)
                    .HasName("PK__PREVISIO__CB895FA4A2529B5C");

                entity.Property(e => e.Idprevision).ValueGeneratedNever();

                entity.Property(e => e.Noescale).IsUnicode(false);

                entity.HasOne(d => d.IdnavireNavigation)
                    .WithMany(p => p.Previsions)
                    .HasForeignKey(d => d.Idnavire)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PREVISION__IDNAV__5CD6CB2B");
            });

            modelBuilder.Entity<Quai>(entity =>
            {
                entity.HasKey(e => e.Idquai)
                    .HasName("PK__QUAI__B53C365AAACDE898");

                entity.Property(e => e.Idquai).ValueGeneratedNever();

                entity.Property(e => e.Nbnavire).HasDefaultValueSql("((1))");

                entity.Property(e => e.Nomquai).IsUnicode(false);
            });

            modelBuilder.Entity<Tarrification>(entity =>
            {
                entity.HasKey(e => e.Idtarrification)
                    .HasName("PK__TARRIFIC__0EA3160CAE7A4335");

                entity.Property(e => e.Idtarrification).ValueGeneratedNever();

                entity.HasOne(d => d.IddeviseNavigation)
                    .WithMany(p => p.Tarrifications)
                    .HasForeignKey(d => d.Iddevise)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TARRIFICA__IDDEV__6477ECF3");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Tarrifications)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TARRIFICA__IDQUA__6383C8BA");
            });

            modelBuilder.Entity<Touche>(entity =>
            {
                entity.HasKey(e => e.Idtouche)
                    .HasName("PK__TOUCHE__9911EE763CAC21EA");

                entity.Property(e => e.Idtouche).ValueGeneratedNever();

                entity.HasOne(d => d.IdprevisionNavigation)
                    .WithMany(p => p.Touches)
                    .HasForeignKey(d => d.Idprevision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TOUCHE__IDPREVIS__5FB337D6");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Touches)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TOUCHE__IDQUAI__60A75C0F");
            });

            modelBuilder.Entity<Tranchepenalite>(entity =>
            {
                entity.HasKey(e => e.Idtranchepenalite)
                    .HasName("PK__TRANCHEP__9338C7A676535412");

                entity.Property(e => e.Idtranchepenalite).ValueGeneratedNever();

                entity.HasOne(d => d.IddeviseNavigation)
                    .WithMany(p => p.Tranchepenalites)
                    .HasForeignKey(d => d.Iddevise)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TRANCHEPE__IDDEV__6B24EA82");

                entity.HasOne(d => d.IdquaiNavigation)
                    .WithMany(p => p.Tranchepenalites)
                    .HasForeignKey(d => d.Idquai)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TRANCHEPE__IDQUA__6A30C649");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
