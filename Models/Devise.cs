﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("DEVISE")]
    public partial class Devise
    {
        public Devise()
        {
            Coursdevises = new HashSet<Coursdevise>();
            Horsplafonds = new HashSet<Horsplafond>();
            Penalitehorsplafonds = new HashSet<Penalitehorsplafond>();
            Plafondpenalites = new HashSet<Plafondpenalite>();
            Plafonds = new HashSet<Plafond>();
            Tarrifications = new HashSet<Tarrification>();
            Tranchepenalites = new HashSet<Tranchepenalite>();
        }

        [Key]
        [Column("IDDEVISE")]
        public int Iddevise { get; set; }
        [Required]
        [Column("NOMDEVISE")]
        [StringLength(20)]
        public string Nomdevise { get; set; }
        [Required]
        [Column("SYMBOL")]
        [StringLength(3)]
        public string Symbol { get; set; }

        [InverseProperty(nameof(Coursdevise.IddeviseNavigation))]
        public virtual ICollection<Coursdevise> Coursdevises { get; set; }
        [InverseProperty(nameof(Horsplafond.IddeviseNavigation))]
        public virtual ICollection<Horsplafond> Horsplafonds { get; set; }
        [InverseProperty(nameof(Penalitehorsplafond.IddeviseNavigation))]
        public virtual ICollection<Penalitehorsplafond> Penalitehorsplafonds { get; set; }
        [InverseProperty(nameof(Plafondpenalite.IddeviseNavigation))]
        public virtual ICollection<Plafondpenalite> Plafondpenalites { get; set; }
        [InverseProperty(nameof(Plafond.IddeviseNavigation))]
        public virtual ICollection<Plafond> Plafonds { get; set; }
        [InverseProperty(nameof(Tarrification.IddeviseNavigation))]
        public virtual ICollection<Tarrification> Tarrifications { get; set; }
        [InverseProperty(nameof(Tranchepenalite.IddeviseNavigation))]
        public virtual ICollection<Tranchepenalite> Tranchepenalites { get; set; }
    }
}
