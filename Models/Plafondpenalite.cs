﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("PLAFONDPENALITE")]
    public partial class Plafondpenalite
    {
        public Plafondpenalite()
        {
            Detailplafondpenalites = new HashSet<Detailplafondpenalite>();
        }

        [Key]
        [Column("IDPLAFONDPENALITE")]
        public int Idplafondpenalite { get; set; }
        [Column("DATE", TypeName = "date")]
        public DateTime Date { get; set; }
        [Column("IDQUAI")]
        public int Idquai { get; set; }
        [Column("IDDEVISE")]
        public int Iddevise { get; set; }

        [ForeignKey(nameof(Iddevise))]
        [InverseProperty(nameof(Devise.Plafondpenalites))]
        public virtual Devise IddeviseNavigation { get; set; }
        [ForeignKey(nameof(Idquai))]
        [InverseProperty(nameof(Quai.Plafondpenalites))]
        public virtual Quai IdquaiNavigation { get; set; }
        [InverseProperty(nameof(Detailplafondpenalite.IdplafondpenaliteNavigation))]
        public virtual ICollection<Detailplafondpenalite> Detailplafondpenalites { get; set; }
    }
}
