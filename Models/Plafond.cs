﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("PLAFOND")]
    public partial class Plafond
    {
        public Plafond()
        {
            Listeplafonds = new HashSet<Listeplafond>();
        }

        [Key]
        [Column("IDPLAFOND")]
        public int Idplafond { get; set; }
        [Column("DATE", TypeName = "date")]
        public DateTime Date { get; set; }
        [Column("IDQUAI")]
        public int Idquai { get; set; }
        [Column("IDDEVISE")]
        public int Iddevise { get; set; }

        [ForeignKey(nameof(Iddevise))]
        [InverseProperty(nameof(Devise.Plafonds))]
        public virtual Devise IddeviseNavigation { get; set; }
        [ForeignKey(nameof(Idquai))]
        [InverseProperty(nameof(Quai.Plafonds))]
        public virtual Quai IdquaiNavigation { get; set; }
        [InverseProperty(nameof(Listeplafond.IdplafondNavigation))]
        public virtual ICollection<Listeplafond> Listeplafonds { get; set; }
    }
}
