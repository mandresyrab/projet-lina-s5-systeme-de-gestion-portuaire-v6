﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("TRANCHEPENALITE")]
    public partial class Tranchepenalite
    {
        [Key]
        [Column("IDTRANCHEPENALITE")]
        public int Idtranchepenalite { get; set; }
        [Column("DATE", TypeName = "date")]
        public DateTime? Date { get; set; }
        [Column("IDQUAI")]
        public int Idquai { get; set; }
        [Column("TRANCHE")]
        public double Tranche { get; set; }
        [Column("TONNAGE")]
        public double Tonnage { get; set; }
        [Column("TARRIF")]
        public double Tarrif { get; set; }
        [Column("IDDEVISE")]
        public int Iddevise { get; set; }

        [ForeignKey(nameof(Iddevise))]
        [InverseProperty(nameof(Devise.Tranchepenalites))]
        public virtual Devise IddeviseNavigation { get; set; }
        [ForeignKey(nameof(Idquai))]
        [InverseProperty(nameof(Quai.Tranchepenalites))]
        public virtual Quai IdquaiNavigation { get; set; }
    }
}
