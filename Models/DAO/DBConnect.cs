﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models.DAO
{
    public class DBConnect
    {
        static SqlConnection c=null;
        public static SqlConnection Connect()
        {
            if (c == null)
            {
                c = new SqlConnection("Data Source=localhost;Initial Catalog=port;Integrated Security=True");
            }
            return c;
        }
    }
}
