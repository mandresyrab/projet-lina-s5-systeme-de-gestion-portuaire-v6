﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("LISTEPLAFOND")]
    public partial class Listeplafond
    {
        [Key]
        [Column("IDLISTEPLAFOND")]
        public int Idlisteplafond { get; set; }
        [Column("IDPLAFOND")]
        public int Idplafond { get; set; }
        [Column("MIN")]
        public double Min { get; set; }
        [Column("MAX")]
        public double Max { get; set; }
        [Column("TARRIF")]
        public double Tarrif { get; set; }
        [Column("TONNAGE")]
        public double Tonnage { get; set; }

        [ForeignKey(nameof(Idplafond))]
        [InverseProperty(nameof(Plafond.Listeplafonds))]
        public virtual Plafond IdplafondNavigation { get; set; }
    }
}
