﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("DEPART")]
    public partial class Depart
    {
        [Key]
        [Column("IDDEPART")]
        public int Iddepart { get; set; }
        [Column("IDPREVISION")]
        public int Idprevision { get; set; }
        [Column("DATE", TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("IDQUAI")]
        public int Idquai { get; set; }
        [Column("DEFINITIF")]
        public bool Definitif { get; set; }

        [ForeignKey(nameof(Idprevision))]
        [InverseProperty(nameof(Prevision.Departs))]
        public virtual Prevision IdprevisionNavigation { get; set; }
        [ForeignKey(nameof(Idquai))]
        [InverseProperty(nameof(Quai.Departs))]
        public virtual Quai IdquaiNavigation { get; set; }
    }
}
