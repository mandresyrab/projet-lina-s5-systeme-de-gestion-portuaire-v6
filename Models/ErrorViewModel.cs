using System;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
