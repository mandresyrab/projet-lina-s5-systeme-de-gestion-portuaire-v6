﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("PREVISION")]
    public partial class Prevision
    {
        static SqlConnection c = Models.DAO.DBConnect.Connect();
        public Prevision()
        {
            Departs = new HashSet<Depart>();
            Detailprevisions = new HashSet<Detailprevision>();
            Touches = new HashSet<Touche>();
        }

        [Key]
        [Column("IDPREVISION")]
        public int Idprevision { get; set; }
        [Required]
        [Column("NOESCALE")]
        [StringLength(10)]
        public string Noescale { get; set; }
        [Column("IDNAVIRE")]
        public int Idnavire { get; set; }

        [ForeignKey(nameof(Idnavire))]
        [InverseProperty(nameof(Navire.Previsions))]
        public virtual Navire IdnavireNavigation { get; set; }
        [InverseProperty(nameof(Depart.IdprevisionNavigation))]
        public virtual ICollection<Depart> Departs { get; set; }
        [InverseProperty(nameof(Detailprevision.IdprevisionNavigation))]
        public virtual ICollection<Detailprevision> Detailprevisions { get; set; }
        [InverseProperty(nameof(Touche.IdprevisionNavigation))]
        public virtual ICollection<Touche> Touches { get; set; }

        public bool SurQuai(int idEx)
        {
            bool open = false;
            try
            {
                if (c.State != System.Data.ConnectionState.Open)
                {
                    c.Open();
                    open = true;
                }
                string sql = $"SELECT * FROM TOUCHE WHERE IDPREVISION={this.Idprevision} AND IDTOUCHE!={idEx} AND IDQUAI NOT IN (SELECT IDQUAI FROM DEPART WHERE IDPREVISION={this.Idprevision})";
                SqlCommand cmd = new SqlCommand(sql, c);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return true;
                }
                return false;
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (open) c.Close();
            }
        }

        public static Prevision FindById(int id)
        {
            bool open = false;
            try
            {
                if (c.State != System.Data.ConnectionState.Open)
                {
                    c.Open();
                    open = true;
                }
                string sql = $"SELECT IDPREVISION , NOESCALE , IDNAVIRE FROM PREVISION WHERE IDPREVISION={id}";
                SqlCommand cmd = new SqlCommand(sql, c);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Prevision prev = new Prevision()
                    {
                        Idprevision=reader.GetInt32(0),
                        Noescale=reader.GetString(1),
                        Idnavire=reader.GetInt32(2)
                    };
                    return prev;
                }
                throw new Exception("Cet Id n'existe pas");
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (open) c.Close();
            }
        }
        public Navire GetNavire()
        {
            return Navire.FindById(this.Idnavire);
        }
    }
}
