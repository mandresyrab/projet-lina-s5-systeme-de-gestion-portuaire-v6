﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("QUAI")]
    public partial class Quai
    {
        static SqlConnection c = Models.DAO.DBConnect.Connect();
        int nbnavire;
        double profondeur;
        public Quai()
        {
            Departs = new HashSet<Depart>();
            Detailprevisions = new HashSet<Detailprevision>();
            Horsplafonds = new HashSet<Horsplafond>();
            Penalitehorsplafonds = new HashSet<Penalitehorsplafond>();
            Plafondpenalites = new HashSet<Plafondpenalite>();
            Plafonds = new HashSet<Plafond>();
            Tarrifications = new HashSet<Tarrification>();
            Touches = new HashSet<Touche>();
            Tranchepenalites = new HashSet<Tranchepenalite>();
        }

        [Key]
        [Column("IDQUAI")]
        public int Idquai { get; set; }
        [Required]
        [Column("NOMQUAI")]
        [StringLength(20)]
        public string Nomquai { get; set; }
        [Column("PROFONDEUR")]
        public double Profondeur {
            get { return profondeur; }
            set
            {
                if (value < 0) throw new Exception("La profondeur du quai ne peut pas être negative");
                profondeur = value;
            }
        }
        [Column("NBNAVIRE")]
        public int Nbnavire {
            get { return nbnavire; }
            set
            {
                if (value < 0) throw new Exception("Le nombre de navire maximale que peut contenir le quai ne peut être négatif");
                nbnavire = value;
            }
        }

        [InverseProperty(nameof(Depart.IdquaiNavigation))]
        public virtual ICollection<Depart> Departs { get; set; }
        [InverseProperty(nameof(Detailprevision.IdquaiNavigation))]
        public virtual ICollection<Detailprevision> Detailprevisions { get; set; }
        [InverseProperty(nameof(Horsplafond.IdquaiNavigation))]
        public virtual ICollection<Horsplafond> Horsplafonds { get; set; }
        [InverseProperty(nameof(Penalitehorsplafond.IdquaiNavigation))]
        public virtual ICollection<Penalitehorsplafond> Penalitehorsplafonds { get; set; }
        [InverseProperty(nameof(Plafondpenalite.IdquaiNavigation))]
        public virtual ICollection<Plafondpenalite> Plafondpenalites { get; set; }
        [InverseProperty(nameof(Plafond.IdquaiNavigation))]
        public virtual ICollection<Plafond> Plafonds { get; set; }
        [InverseProperty(nameof(Tarrification.IdquaiNavigation))]
        public virtual ICollection<Tarrification> Tarrifications { get; set; }
        [InverseProperty(nameof(Touche.IdquaiNavigation))]
        public virtual ICollection<Touche> Touches { get; set; }
        [InverseProperty(nameof(Tranchepenalite.IdquaiNavigation))]
        public virtual ICollection<Tranchepenalite> Tranchepenalites { get; set; }

        public bool Libre()
        {
            bool open = false;
            try
            {
                if (c.State != System.Data.ConnectionState.Open)
                {
                    c.Open();
                    open = true;
                }
                string sql = $"SELECT * FROM TOUCHE WHERE IDQUAI={this.Idquai} AND IDPREVISION NOT IN (SELECT IDPREVISION FROM DEPART WHERE IDQUAI={this.Idquai})";
                SqlCommand cmd = new SqlCommand(sql, c);
                SqlDataReader reader = cmd.ExecuteReader();
                int nb = 0;
                while (reader.Read())
                {
                    nb++;
                    if (nb >= this.Nbnavire) return false;
                }
                return true;
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (open) c.Close();
            }
        }

        public static Quai FindById(int id)
        {
            bool open = false;
            try
            {
                if (c.State != System.Data.ConnectionState.Open)
                {
                    c.Open();
                    open = true;
                }
                string sql = $"SELECT IDQUAI , NOMQUAI , PROFONDEUR , NBNAVIRE FROM QUAI WHERE IDQUAI = {id}";
                SqlCommand cmd = new SqlCommand(sql, c);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Quai quai = new Quai()
                    {
                        Idquai=reader.GetInt32(0),
                        Nomquai=reader.GetString(1),
                        Profondeur=reader.GetDouble(2),
                        Nbnavire=reader.GetInt32(3)
                    };
                    return quai;
                }
                throw new Exception("Cet ID n'existe pas dans la base de donné");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (open) c.Close();
            }
        }
    }
}
