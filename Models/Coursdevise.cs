﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("COURSDEVISE")]
    public partial class Coursdevise
    {
        double ariary;
        [Key]
        [Column("IDCOURSDEVISE")]
        public int Idcoursdevise { get; set; }
        [Column("IDDEVISE")]
        public int Iddevise { get; set; }
        [Column("DATE", TypeName = "date")]
        public DateTime Date { get; set; }
        [Column("ARIARY")]
        public double Ariary { 
            get { return ariary; }
            set
            {
                if (value < 0) throw new Exception("La valeur de cours ne doit jamais etre negative");
                ariary = value;
            }
        }

        [ForeignKey(nameof(Iddevise))]
        [InverseProperty(nameof(Devise.Coursdevises))]
        public virtual Devise IddeviseNavigation { get; set; }
    }
}
