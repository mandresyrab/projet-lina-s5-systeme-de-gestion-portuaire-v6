﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("NAVIRE")]
    public partial class Navire
    {
        static SqlConnection c = Models.DAO.DBConnect.Connect(); 
        double tonnage;
        double profondeur;

        public Navire()
        {
            Previsions = new HashSet<Prevision>();
        }

        [Key]
        [Column("IDNAVIRE")]
        public int Idnavire { get; set; }
        [Required]
        [Column("NOMNAVIRE")]
        [StringLength(20)]
        public string Nomnavire { get; set; }
        [Column("TONNAGE")]
        public double Tonnage {
            get
            {
                return tonnage;
            }
            set
            {
                if (value < 0) throw new Exception("Le tonnage du navire doit toujours être positif");
                tonnage = value;
            } 
        }
        [Column("PROFONDEUR")]
        public double Profondeur {
            get { return profondeur; }
            set
            {
                if (value < 0) throw new Exception("La profondeur du navire doit toujours être positif");
                profondeur = value;
            }
        }

        [InverseProperty(nameof(Prevision.IdnavireNavigation))]
        public virtual ICollection<Prevision> Previsions { get; set; }

        

        public static Navire FindById(int id)
        {
            bool open = false;
            try
            {
                if(c.State!=System.Data.ConnectionState.Open)
                {
                    c.Open();
                    open = true;
                }
                string sql = $"SELECT IDNAVIRE , NOMNAVIRE , TONNAGE , PROFONDEUR FROM NAVIRE WHERE IDNAVIRE = {id}";
                SqlCommand cmd = new SqlCommand(sql, c);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Navire nav = new Navire()
                    {
                        Idnavire = reader.GetInt32(0),
                        Nomnavire = reader.GetString(1),
                        Tonnage = reader.GetDouble(2),
                        Profondeur = reader.GetDouble(3)
                    };
                    return nav;
                }
                throw new Exception("Cet ID n'existe pas dans la base de donné");
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (open) c.Close();
            }
        }
    }
}
