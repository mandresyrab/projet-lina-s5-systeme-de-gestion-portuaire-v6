﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("DETAILPLAFONDPENALITE")]
    public partial class Detailplafondpenalite
    {
        static SqlConnection c=Models.DAO.DBConnect.Connect();
        double min = -1;
        double max = -1;
        double tarrif;
        double tonnage;
        [Key]
        [Column("IDDETAILPLAFONDPENALITE")]
        public int Iddetailplafondpenalite { get; set; }
        [Column("IDPLAFONDPENALITE")]
        public int Idplafondpenalite { get; set; }
        [Column("MIN")]
        public double Min { 
            get { return (min==-1)?0:min; }
            set
            {
                if (value < 0) throw new Exception("Minimum négatif : valeur invalide");
                if (max != -1 && value > max) throw new Exception("Minimum > Maximum : Invalide");
                min = value;
            }
        }
        [Column("MAX")]
        public double Max {
            get { return (max == -1) ? 0 : min; }
            set
            {
                if (value < 0) throw new Exception("Maximum négatif : valeur invalide");
                if (min != -1 && value < min) throw new Exception("Minimum > Maximum : Invalide");
                max = value;
            }
        }
        [Column("TARRIF")]
        public double Tarrif {
            get { return tarrif; }
            set
            {
                if (value < 0) throw new Exception("Tarrif négatif : Invalide");
                tarrif = value;
            }
        }
        [Column("TONNAGE")]
        public double Tonnage { get; set; }

        [ForeignKey(nameof(Idplafondpenalite))]
        [InverseProperty(nameof(Plafondpenalite.Detailplafondpenalites))]
        public virtual Plafondpenalite IdplafondpenaliteNavigation { get; set; }

        public static List<Detailplafondpenalite> FindByPlafondPenalite(int idPenalite)
        {
            bool open = false;
            try
            {
                if (c.State != System.Data.ConnectionState.Open)
                {
                    c.Open();
                    open = true;
                }
                string sql = $"SELECT IDDETAILPLAFONDPENALITE, IDPLAFONDPENALITE, MIN, MAX, TARRIF, TONNAGE FROM DETAILPLAFONDPENALITE WHERE IDPLAFONDPENALITE = {idPenalite}";
                SqlCommand cmd = new SqlCommand(sql, c);
                List<Detailplafondpenalite> result = new List<Detailplafondpenalite>();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new Detailplafondpenalite()
                    {
                        Iddetailplafondpenalite=reader.GetInt32(0),
                        Idplafondpenalite=idPenalite,
                        Min=reader.GetDouble(2),
                        Max=reader.GetDouble(3),
                        Tarrif=reader.GetDouble(4),
                        Tonnage=reader.GetDouble(5)
                    });
                }
                return result;
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (open) c.Close();
            }
        }
    }
}
