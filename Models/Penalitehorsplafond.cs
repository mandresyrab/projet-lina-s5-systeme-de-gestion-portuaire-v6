﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models
{
    [Table("PENALITEHORSPLAFOND")]
    public partial class Penalitehorsplafond
    {
        [Key]
        [Column("IDPENALITEHORSPLAFOND")]
        public int Idpenalitehorsplafond { get; set; }
        [Column("DATE", TypeName = "date")]
        public DateTime Date { get; set; }
        [Column("IDQUAI")]
        public int Idquai { get; set; }
        [Column("TARRIF")]
        public double Tarrif { get; set; }
        [Column("TONNAGE")]
        public double Tonnage { get; set; }
        [Column("TRANCHE")]
        public double Tranche { get; set; }
        [Column("IDDEVISE")]
        public int Iddevise { get; set; }

        [ForeignKey(nameof(Iddevise))]
        [InverseProperty(nameof(Devise.Penalitehorsplafonds))]
        public virtual Devise IddeviseNavigation { get; set; }
        [ForeignKey(nameof(Idquai))]
        [InverseProperty(nameof(Quai.Penalitehorsplafonds))]
        public virtual Quai IdquaiNavigation { get; set; }
    }
}
