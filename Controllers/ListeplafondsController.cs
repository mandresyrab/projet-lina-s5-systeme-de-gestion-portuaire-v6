﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class ListeplafondsController : Controller
    {
        private readonly DBContext _context;

        public ListeplafondsController(DBContext context)
        {
            _context = context;
        }

        // GET: Listeplafonds
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Listeplafonds.Include(l => l.IdplafondNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Listeplafonds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listeplafond = await _context.Listeplafonds
                .Include(l => l.IdplafondNavigation)
                .FirstOrDefaultAsync(m => m.Idlisteplafond == id);
            if (listeplafond == null)
            {
                return NotFound();
            }

            return View(listeplafond);
        }

        // GET: Listeplafonds/Create
        public IActionResult Create()
        {
            ViewData["Idplafond"] = new SelectList(_context.Plafonds, "Idplafond", "Idplafond");
            return View();
        }

        // POST: Listeplafonds/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idlisteplafond,Idplafond,Min,Max,Tarrif,Tonnage")] Listeplafond listeplafond)
        {
            if (ModelState.IsValid)
            {
                _context.Add(listeplafond);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idplafond"] = new SelectList(_context.Plafonds, "Idplafond", "Idplafond", listeplafond.Idplafond);
            return View(listeplafond);
        }

        // GET: Listeplafonds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listeplafond = await _context.Listeplafonds.FindAsync(id);
            if (listeplafond == null)
            {
                return NotFound();
            }
            ViewData["Idplafond"] = new SelectList(_context.Plafonds, "Idplafond", "Idplafond", listeplafond.Idplafond);
            return View(listeplafond);
        }

        // POST: Listeplafonds/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idlisteplafond,Idplafond,Min,Max,Tarrif,Tonnage")] Listeplafond listeplafond)
        {
            if (id != listeplafond.Idlisteplafond)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(listeplafond);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ListeplafondExists(listeplafond.Idlisteplafond))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idplafond"] = new SelectList(_context.Plafonds, "Idplafond", "Idplafond", listeplafond.Idplafond);
            return View(listeplafond);
        }

        // GET: Listeplafonds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listeplafond = await _context.Listeplafonds
                .Include(l => l.IdplafondNavigation)
                .FirstOrDefaultAsync(m => m.Idlisteplafond == id);
            if (listeplafond == null)
            {
                return NotFound();
            }

            return View(listeplafond);
        }

        // POST: Listeplafonds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var listeplafond = await _context.Listeplafonds.FindAsync(id);
            _context.Listeplafonds.Remove(listeplafond);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ListeplafondExists(int id)
        {
            return _context.Listeplafonds.Any(e => e.Idlisteplafond == id);
        }
    }
}
