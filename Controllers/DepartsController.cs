﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class DepartsController : Controller
    {
        private readonly DBContext _context;

        public DepartsController(DBContext context)
        {
            _context = context;
        }

        // GET: Departs
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Departs.Include(d => d.IdprevisionNavigation).Include(d => d.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Departs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depart = await _context.Departs
                .Include(d => d.IdprevisionNavigation)
                .Include(d => d.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Iddepart == id);
            if (depart == null)
            {
                return NotFound();
            }

            return View(depart);
        }

        // GET: Departs/Create
        public IActionResult Create()
        {
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Departs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Iddepart,Idprevision,Date,Idquai,Definitif")] Depart depart)
        {
            if (ModelState.IsValid)
            {
                _context.Add(depart);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", depart.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", depart.Idquai);
            return View(depart);
        }

        // GET: Departs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depart = await _context.Departs.FindAsync(id);
            if (depart == null)
            {
                return NotFound();
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", depart.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", depart.Idquai);
            return View(depart);
        }

        // POST: Departs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Iddepart,Idprevision,Date,Idquai,Definitif")] Depart depart)
        {
            if (id != depart.Iddepart)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(depart);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartExists(depart.Iddepart))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", depart.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", depart.Idquai);
            return View(depart);
        }

        // GET: Departs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depart = await _context.Departs
                .Include(d => d.IdprevisionNavigation)
                .Include(d => d.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Iddepart == id);
            if (depart == null)
            {
                return NotFound();
            }

            return View(depart);
        }

        // POST: Departs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var depart = await _context.Departs.FindAsync(id);
            _context.Departs.Remove(depart);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepartExists(int id)
        {
            return _context.Departs.Any(e => e.Iddepart == id);
        }
    }
}
