﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class QuaisController : Controller
    {
        private readonly DBContext _context;

        public QuaisController(DBContext context)
        {
            _context = context;
        }

        // GET: Quais
        public async Task<IActionResult> Index()
        {
            return View(await _context.Quais.ToListAsync());
        }

        // GET: Quais/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quai = await _context.Quais
                .FirstOrDefaultAsync(m => m.Idquai == id);
            if (quai == null)
            {
                return NotFound();
            }

            return View(quai);
        }

        // GET: Quais/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Quais/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idquai,Nomquai,Profondeur,Nbnavire")] Quai quai)
        {
            if (ModelState.IsValid)
            {
                _context.Add(quai);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(quai);
        }

        // GET: Quais/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quai = await _context.Quais.FindAsync(id);
            if (quai == null)
            {
                return NotFound();
            }
            return View(quai);
        }

        // POST: Quais/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idquai,Nomquai,Profondeur,Nbnavire")] Quai quai)
        {
            if (id != quai.Idquai)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quai);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuaiExists(quai.Idquai))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(quai);
        }

        // GET: Quais/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quai = await _context.Quais
                .FirstOrDefaultAsync(m => m.Idquai == id);
            if (quai == null)
            {
                return NotFound();
            }

            return View(quai);
        }

        // POST: Quais/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var quai = await _context.Quais.FindAsync(id);
            _context.Quais.Remove(quai);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuaiExists(int id)
        {
            return _context.Quais.Any(e => e.Idquai == id);
        }
    }
}
