﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class PenalitehorsplafondsController : Controller
    {
        private readonly DBContext _context;

        public PenalitehorsplafondsController(DBContext context)
        {
            _context = context;
        }

        // GET: Penalitehorsplafonds
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Penalitehorsplafonds.Include(p => p.IddeviseNavigation).Include(p => p.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Penalitehorsplafonds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var penalitehorsplafond = await _context.Penalitehorsplafonds
                .Include(p => p.IddeviseNavigation)
                .Include(p => p.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idpenalitehorsplafond == id);
            if (penalitehorsplafond == null)
            {
                return NotFound();
            }

            return View(penalitehorsplafond);
        }

        // GET: Penalitehorsplafonds/Create
        public IActionResult Create()
        {
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Penalitehorsplafonds/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idpenalitehorsplafond,Date,Idquai,Tarrif,Tonnage,Tranche,Iddevise")] Penalitehorsplafond penalitehorsplafond)
        {
            if (ModelState.IsValid)
            {
                _context.Add(penalitehorsplafond);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", penalitehorsplafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", penalitehorsplafond.Idquai);
            return View(penalitehorsplafond);
        }

        // GET: Penalitehorsplafonds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var penalitehorsplafond = await _context.Penalitehorsplafonds.FindAsync(id);
            if (penalitehorsplafond == null)
            {
                return NotFound();
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", penalitehorsplafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", penalitehorsplafond.Idquai);
            return View(penalitehorsplafond);
        }

        // POST: Penalitehorsplafonds/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idpenalitehorsplafond,Date,Idquai,Tarrif,Tonnage,Tranche,Iddevise")] Penalitehorsplafond penalitehorsplafond)
        {
            if (id != penalitehorsplafond.Idpenalitehorsplafond)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(penalitehorsplafond);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PenalitehorsplafondExists(penalitehorsplafond.Idpenalitehorsplafond))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", penalitehorsplafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", penalitehorsplafond.Idquai);
            return View(penalitehorsplafond);
        }

        // GET: Penalitehorsplafonds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var penalitehorsplafond = await _context.Penalitehorsplafonds
                .Include(p => p.IddeviseNavigation)
                .Include(p => p.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idpenalitehorsplafond == id);
            if (penalitehorsplafond == null)
            {
                return NotFound();
            }

            return View(penalitehorsplafond);
        }

        // POST: Penalitehorsplafonds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var penalitehorsplafond = await _context.Penalitehorsplafonds.FindAsync(id);
            _context.Penalitehorsplafonds.Remove(penalitehorsplafond);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PenalitehorsplafondExists(int id)
        {
            return _context.Penalitehorsplafonds.Any(e => e.Idpenalitehorsplafond == id);
        }
    }
}
