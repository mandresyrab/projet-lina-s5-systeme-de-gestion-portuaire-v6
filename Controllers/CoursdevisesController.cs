﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class CoursdevisesController : Controller
    {
        private readonly DBContext _context;

        public CoursdevisesController(DBContext context)
        {
            _context = context;
        }

        // GET: Coursdevises
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Coursdevises.Include(c => c.IddeviseNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Coursdevises/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coursdevise = await _context.Coursdevises
                .Include(c => c.IddeviseNavigation)
                .FirstOrDefaultAsync(m => m.Idcoursdevise == id);
            if (coursdevise == null)
            {
                return NotFound();
            }

            return View(coursdevise);
        }

        // GET: Coursdevises/Create
        public IActionResult Create()
        {
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise");
            return View();
        }

        // POST: Coursdevises/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idcoursdevise,Iddevise,Date,Ariary")] Coursdevise coursdevise)
        {
            if (ModelState.IsValid)
            {
                _context.Add(coursdevise);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", coursdevise.Iddevise);
            return View(coursdevise);
        }

        // GET: Coursdevises/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coursdevise = await _context.Coursdevises.FindAsync(id);
            if (coursdevise == null)
            {
                return NotFound();
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", coursdevise.Iddevise);
            return View(coursdevise);
        }

        // POST: Coursdevises/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idcoursdevise,Iddevise,Date,Ariary")] Coursdevise coursdevise)
        {
            if (id != coursdevise.Idcoursdevise)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(coursdevise);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CoursdeviseExists(coursdevise.Idcoursdevise))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", coursdevise.Iddevise);
            return View(coursdevise);
        }

        // GET: Coursdevises/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coursdevise = await _context.Coursdevises
                .Include(c => c.IddeviseNavigation)
                .FirstOrDefaultAsync(m => m.Idcoursdevise == id);
            if (coursdevise == null)
            {
                return NotFound();
            }

            return View(coursdevise);
        }

        // POST: Coursdevises/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var coursdevise = await _context.Coursdevises.FindAsync(id);
            _context.Coursdevises.Remove(coursdevise);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CoursdeviseExists(int id)
        {
            return _context.Coursdevises.Any(e => e.Idcoursdevise == id);
        }
    }
}
