﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class TarrificationsController : Controller
    {
        private readonly DBContext _context;

        public TarrificationsController(DBContext context)
        {
            _context = context;
        }

        // GET: Tarrifications
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Tarrifications.Include(t => t.IddeviseNavigation).Include(t => t.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Tarrifications/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tarrification = await _context.Tarrifications
                .Include(t => t.IddeviseNavigation)
                .Include(t => t.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idtarrification == id);
            if (tarrification == null)
            {
                return NotFound();
            }

            return View(tarrification);
        }

        // GET: Tarrifications/Create
        public IActionResult Create()
        {
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Tarrifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idtarrification,Idquai,Date,Tranche,Tarrif,Iddevise,Tonnage")] Tarrification tarrification)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tarrification);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", tarrification.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", tarrification.Idquai);
            return View(tarrification);
        }

        // GET: Tarrifications/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tarrification = await _context.Tarrifications.FindAsync(id);
            if (tarrification == null)
            {
                return NotFound();
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", tarrification.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", tarrification.Idquai);
            return View(tarrification);
        }

        // POST: Tarrifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idtarrification,Idquai,Date,Tranche,Tarrif,Iddevise,Tonnage")] Tarrification tarrification)
        {
            if (id != tarrification.Idtarrification)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tarrification);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TarrificationExists(tarrification.Idtarrification))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", tarrification.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", tarrification.Idquai);
            return View(tarrification);
        }

        // GET: Tarrifications/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tarrification = await _context.Tarrifications
                .Include(t => t.IddeviseNavigation)
                .Include(t => t.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idtarrification == id);
            if (tarrification == null)
            {
                return NotFound();
            }

            return View(tarrification);
        }

        // POST: Tarrifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tarrification = await _context.Tarrifications.FindAsync(id);
            _context.Tarrifications.Remove(tarrification);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TarrificationExists(int id)
        {
            return _context.Tarrifications.Any(e => e.Idtarrification == id);
        }
    }
}
