﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class DevisesController : Controller
    {
        private readonly DBContext _context;

        public DevisesController(DBContext context)
        {
            _context = context;
        }

        // GET: Devises
        public async Task<IActionResult> Index()
        {
            return View(await _context.Devises.ToListAsync());
        }

        // GET: Devises/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var devise = await _context.Devises
                .FirstOrDefaultAsync(m => m.Iddevise == id);
            if (devise == null)
            {
                return NotFound();
            }

            return View(devise);
        }

        // GET: Devises/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Devises/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Iddevise,Nomdevise,Symbol")] Devise devise)
        {
            if (ModelState.IsValid)
            {
                _context.Add(devise);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(devise);
        }

        // GET: Devises/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var devise = await _context.Devises.FindAsync(id);
            if (devise == null)
            {
                return NotFound();
            }
            return View(devise);
        }

        // POST: Devises/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Iddevise,Nomdevise,Symbol")] Devise devise)
        {
            if (id != devise.Iddevise)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(devise);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeviseExists(devise.Iddevise))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(devise);
        }

        // GET: Devises/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var devise = await _context.Devises
                .FirstOrDefaultAsync(m => m.Iddevise == id);
            if (devise == null)
            {
                return NotFound();
            }

            return View(devise);
        }

        // POST: Devises/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var devise = await _context.Devises.FindAsync(id);
            _context.Devises.Remove(devise);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeviseExists(int id)
        {
            return _context.Devises.Any(e => e.Iddevise == id);
        }
    }
}
