﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class HorsplafondsController : Controller
    {
        private readonly DBContext _context;

        public HorsplafondsController(DBContext context)
        {
            _context = context;
        }

        // GET: Horsplafonds
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Horsplafonds.Include(h => h.IddeviseNavigation).Include(h => h.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Horsplafonds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horsplafond = await _context.Horsplafonds
                .Include(h => h.IddeviseNavigation)
                .Include(h => h.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idhorsplafond == id);
            if (horsplafond == null)
            {
                return NotFound();
            }

            return View(horsplafond);
        }

        // GET: Horsplafonds/Create
        public IActionResult Create()
        {
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Horsplafonds/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idhorsplafond,Date,Idquai,Tarrif,Tonnage,Tranche,Iddevise")] Horsplafond horsplafond)
        {
            if (ModelState.IsValid)
            {
                _context.Add(horsplafond);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", horsplafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", horsplafond.Idquai);
            return View(horsplafond);
        }

        // GET: Horsplafonds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horsplafond = await _context.Horsplafonds.FindAsync(id);
            if (horsplafond == null)
            {
                return NotFound();
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", horsplafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", horsplafond.Idquai);
            return View(horsplafond);
        }

        // POST: Horsplafonds/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idhorsplafond,Date,Idquai,Tarrif,Tonnage,Tranche,Iddevise")] Horsplafond horsplafond)
        {
            if (id != horsplafond.Idhorsplafond)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(horsplafond);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HorsplafondExists(horsplafond.Idhorsplafond))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", horsplafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", horsplafond.Idquai);
            return View(horsplafond);
        }

        // GET: Horsplafonds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horsplafond = await _context.Horsplafonds
                .Include(h => h.IddeviseNavigation)
                .Include(h => h.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idhorsplafond == id);
            if (horsplafond == null)
            {
                return NotFound();
            }

            return View(horsplafond);
        }

        // POST: Horsplafonds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var horsplafond = await _context.Horsplafonds.FindAsync(id);
            _context.Horsplafonds.Remove(horsplafond);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HorsplafondExists(int id)
        {
            return _context.Horsplafonds.Any(e => e.Idhorsplafond == id);
        }
    }
}
