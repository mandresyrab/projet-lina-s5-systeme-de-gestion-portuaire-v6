﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class TouchesController : Controller
    {
        private readonly DBContext _context;

        public TouchesController(DBContext context)
        {
            _context = context;
        }

        // GET: Touches
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Touches.Include(t => t.IdprevisionNavigation).Include(t => t.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Touches/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var touche = await _context.Touches
                .Include(t => t.IdprevisionNavigation)
                .Include(t => t.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idtouche == id);
            if (touche == null)
            {
                return NotFound();
            }

            return View(touche);
        }

        // GET: Touches/Create
        public IActionResult Create()
        {
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Touches/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idtouche,Idprevision,Idquai,Date")] Touche touche)
        {
            if (ModelState.IsValid)
            {
                _context.Add(touche);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", touche.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", touche.Idquai);
            return View(touche);
        }

        // GET: Touches/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var touche = await _context.Touches.FindAsync(id);
            if (touche == null)
            {
                return NotFound();
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", touche.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", touche.Idquai);
            return View(touche);
        }

        // POST: Touches/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idtouche,Idprevision,Idquai,Date")] Touche touche)
        {
            if (id != touche.Idtouche)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(touche);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToucheExists(touche.Idtouche))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", touche.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", touche.Idquai);
            return View(touche);
        }

        // GET: Touches/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var touche = await _context.Touches
                .Include(t => t.IdprevisionNavigation)
                .Include(t => t.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idtouche == id);
            if (touche == null)
            {
                return NotFound();
            }

            return View(touche);
        }

        // POST: Touches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var touche = await _context.Touches.FindAsync(id);
            _context.Touches.Remove(touche);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToucheExists(int id)
        {
            return _context.Touches.Any(e => e.Idtouche == id);
        }
    }
}
