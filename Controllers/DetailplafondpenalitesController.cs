﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class DetailplafondpenalitesController : Controller
    {
        private readonly DBContext _context;

        public DetailplafondpenalitesController(DBContext context)
        {
            _context = context;
        }

        // GET: Detailplafondpenalites
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Detailplafondpenalites.Include(d => d.IdplafondpenaliteNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Detailplafondpenalites/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailplafondpenalite = await _context.Detailplafondpenalites
                .Include(d => d.IdplafondpenaliteNavigation)
                .FirstOrDefaultAsync(m => m.Iddetailplafondpenalite == id);
            if (detailplafondpenalite == null)
            {
                return NotFound();
            }

            return View(detailplafondpenalite);
        }

        // GET: Detailplafondpenalites/Create
        public IActionResult Create()
        {
            ViewData["Idplafondpenalite"] = new SelectList(_context.Plafondpenalites, "Idplafondpenalite", "Idplafondpenalite");
            return View();
        }

        // POST: Detailplafondpenalites/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Iddetailplafondpenalite,Idplafondpenalite,Min,Max,Tarrif,Tonnage")] Detailplafondpenalite detailplafondpenalite)
        {
            if (ModelState.IsValid)
            {
                _context.Add(detailplafondpenalite);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idplafondpenalite"] = new SelectList(_context.Plafondpenalites, "Idplafondpenalite", "Idplafondpenalite", detailplafondpenalite.Idplafondpenalite);
            return View(detailplafondpenalite);
        }

        // GET: Detailplafondpenalites/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailplafondpenalite = await _context.Detailplafondpenalites.FindAsync(id);
            if (detailplafondpenalite == null)
            {
                return NotFound();
            }
            ViewData["Idplafondpenalite"] = new SelectList(_context.Plafondpenalites, "Idplafondpenalite", "Idplafondpenalite", detailplafondpenalite.Idplafondpenalite);
            return View(detailplafondpenalite);
        }

        // POST: Detailplafondpenalites/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Iddetailplafondpenalite,Idplafondpenalite,Min,Max,Tarrif,Tonnage")] Detailplafondpenalite detailplafondpenalite)
        {
            if (id != detailplafondpenalite.Iddetailplafondpenalite)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(detailplafondpenalite);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DetailplafondpenaliteExists(detailplafondpenalite.Iddetailplafondpenalite))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idplafondpenalite"] = new SelectList(_context.Plafondpenalites, "Idplafondpenalite", "Idplafondpenalite", detailplafondpenalite.Idplafondpenalite);
            return View(detailplafondpenalite);
        }

        // GET: Detailplafondpenalites/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailplafondpenalite = await _context.Detailplafondpenalites
                .Include(d => d.IdplafondpenaliteNavigation)
                .FirstOrDefaultAsync(m => m.Iddetailplafondpenalite == id);
            if (detailplafondpenalite == null)
            {
                return NotFound();
            }

            return View(detailplafondpenalite);
        }

        // POST: Detailplafondpenalites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var detailplafondpenalite = await _context.Detailplafondpenalites.FindAsync(id);
            _context.Detailplafondpenalites.Remove(detailplafondpenalite);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DetailplafondpenaliteExists(int id)
        {
            return _context.Detailplafondpenalites.Any(e => e.Iddetailplafondpenalite == id);
        }
    }
}
