﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class TranchepenalitesController : Controller
    {
        private readonly DBContext _context;

        public TranchepenalitesController(DBContext context)
        {
            _context = context;
        }

        // GET: Tranchepenalites
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Tranchepenalites.Include(t => t.IddeviseNavigation).Include(t => t.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Tranchepenalites/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tranchepenalite = await _context.Tranchepenalites
                .Include(t => t.IddeviseNavigation)
                .Include(t => t.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idtranchepenalite == id);
            if (tranchepenalite == null)
            {
                return NotFound();
            }

            return View(tranchepenalite);
        }

        // GET: Tranchepenalites/Create
        public IActionResult Create()
        {
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Tranchepenalites/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idtranchepenalite,Date,Idquai,Tranche,Tonnage,Tarrif,Iddevise")] Tranchepenalite tranchepenalite)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tranchepenalite);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", tranchepenalite.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", tranchepenalite.Idquai);
            return View(tranchepenalite);
        }

        // GET: Tranchepenalites/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tranchepenalite = await _context.Tranchepenalites.FindAsync(id);
            if (tranchepenalite == null)
            {
                return NotFound();
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", tranchepenalite.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", tranchepenalite.Idquai);
            return View(tranchepenalite);
        }

        // POST: Tranchepenalites/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idtranchepenalite,Date,Idquai,Tranche,Tonnage,Tarrif,Iddevise")] Tranchepenalite tranchepenalite)
        {
            if (id != tranchepenalite.Idtranchepenalite)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tranchepenalite);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TranchepenaliteExists(tranchepenalite.Idtranchepenalite))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", tranchepenalite.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", tranchepenalite.Idquai);
            return View(tranchepenalite);
        }

        // GET: Tranchepenalites/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tranchepenalite = await _context.Tranchepenalites
                .Include(t => t.IddeviseNavigation)
                .Include(t => t.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idtranchepenalite == id);
            if (tranchepenalite == null)
            {
                return NotFound();
            }

            return View(tranchepenalite);
        }

        // POST: Tranchepenalites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tranchepenalite = await _context.Tranchepenalites.FindAsync(id);
            _context.Tranchepenalites.Remove(tranchepenalite);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TranchepenaliteExists(int id)
        {
            return _context.Tranchepenalites.Any(e => e.Idtranchepenalite == id);
        }
    }
}
