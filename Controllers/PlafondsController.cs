﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class PlafondsController : Controller
    {
        private readonly DBContext _context;

        public PlafondsController(DBContext context)
        {
            _context = context;
        }

        // GET: Plafonds
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Plafonds.Include(p => p.IddeviseNavigation).Include(p => p.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Plafonds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plafond = await _context.Plafonds
                .Include(p => p.IddeviseNavigation)
                .Include(p => p.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idplafond == id);
            if (plafond == null)
            {
                return NotFound();
            }

            return View(plafond);
        }

        // GET: Plafonds/Create
        public IActionResult Create()
        {
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Plafonds/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idplafond,Date,Idquai,Iddevise")] Plafond plafond)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plafond);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", plafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", plafond.Idquai);
            return View(plafond);
        }

        // GET: Plafonds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plafond = await _context.Plafonds.FindAsync(id);
            if (plafond == null)
            {
                return NotFound();
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", plafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", plafond.Idquai);
            return View(plafond);
        }

        // POST: Plafonds/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idplafond,Date,Idquai,Iddevise")] Plafond plafond)
        {
            if (id != plafond.Idplafond)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plafond);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlafondExists(plafond.Idplafond))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", plafond.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", plafond.Idquai);
            return View(plafond);
        }

        // GET: Plafonds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plafond = await _context.Plafonds
                .Include(p => p.IddeviseNavigation)
                .Include(p => p.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idplafond == id);
            if (plafond == null)
            {
                return NotFound();
            }

            return View(plafond);
        }

        // POST: Plafonds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plafond = await _context.Plafonds.FindAsync(id);
            _context.Plafonds.Remove(plafond);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlafondExists(int id)
        {
            return _context.Plafonds.Any(e => e.Idplafond == id);
        }
    }
}
