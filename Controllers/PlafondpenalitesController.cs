﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class PlafondpenalitesController : Controller
    {
        private readonly DBContext _context;

        public PlafondpenalitesController(DBContext context)
        {
            _context = context;
        }

        // GET: Plafondpenalites
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Plafondpenalites.Include(p => p.IddeviseNavigation).Include(p => p.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Plafondpenalites/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plafondpenalite = await _context.Plafondpenalites
                .Include(p => p.IddeviseNavigation)
                .Include(p => p.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idplafondpenalite == id);
            if (plafondpenalite == null)
            {
                return NotFound();
            }

            return View(plafondpenalite);
        }

        // GET: Plafondpenalites/Create
        public IActionResult Create()
        {
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Plafondpenalites/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idplafondpenalite,Date,Idquai,Iddevise")] Plafondpenalite plafondpenalite)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plafondpenalite);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", plafondpenalite.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", plafondpenalite.Idquai);
            return View(plafondpenalite);
        }

        // GET: Plafondpenalites/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plafondpenalite = await _context.Plafondpenalites.FindAsync(id);
            if (plafondpenalite == null)
            {
                return NotFound();
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", plafondpenalite.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", plafondpenalite.Idquai);
            return View(plafondpenalite);
        }

        // POST: Plafondpenalites/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idplafondpenalite,Date,Idquai,Iddevise")] Plafondpenalite plafondpenalite)
        {
            if (id != plafondpenalite.Idplafondpenalite)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plafondpenalite);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlafondpenaliteExists(plafondpenalite.Idplafondpenalite))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iddevise"] = new SelectList(_context.Devises, "Iddevise", "Nomdevise", plafondpenalite.Iddevise);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", plafondpenalite.Idquai);
            return View(plafondpenalite);
        }

        // GET: Plafondpenalites/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plafondpenalite = await _context.Plafondpenalites
                .Include(p => p.IddeviseNavigation)
                .Include(p => p.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Idplafondpenalite == id);
            if (plafondpenalite == null)
            {
                return NotFound();
            }

            return View(plafondpenalite);
        }

        // POST: Plafondpenalites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plafondpenalite = await _context.Plafondpenalites.FindAsync(id);
            _context.Plafondpenalites.Remove(plafondpenalite);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlafondpenaliteExists(int id)
        {
            return _context.Plafondpenalites.Any(e => e.Idplafondpenalite == id);
        }
    }
}
