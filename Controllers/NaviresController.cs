﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class NaviresController : Controller
    {
        private readonly DBContext _context;

        public NaviresController(DBContext context)
        {
            _context = context;
        }

        // GET: Navires
        public async Task<IActionResult> Index()
        {
            return View(await _context.Navires.ToListAsync());
        }

        // GET: Navires/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var navire = await _context.Navires
                .FirstOrDefaultAsync(m => m.Idnavire == id);
            if (navire == null)
            {
                return NotFound();
            }

            return View(navire);
        }

        // GET: Navires/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Navires/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idnavire,Nomnavire,Tonnage,Profondeur")] Navire navire)
        {
            if (ModelState.IsValid)
            {
                _context.Add(navire);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(navire);
        }

        // GET: Navires/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var navire = await _context.Navires.FindAsync(id);
            if (navire == null)
            {
                return NotFound();
            }
            return View(navire);
        }

        // POST: Navires/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idnavire,Nomnavire,Tonnage,Profondeur")] Navire navire)
        {
            if (id != navire.Idnavire)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(navire);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NavireExists(navire.Idnavire))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(navire);
        }

        // GET: Navires/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var navire = await _context.Navires
                .FirstOrDefaultAsync(m => m.Idnavire == id);
            if (navire == null)
            {
                return NotFound();
            }

            return View(navire);
        }

        // POST: Navires/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var navire = await _context.Navires.FindAsync(id);
            _context.Navires.Remove(navire);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NavireExists(int id)
        {
            return _context.Navires.Any(e => e.Idnavire == id);
        }
    }
}
