﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class DetailprevisionsController : Controller
    {
        private readonly DBContext _context;

        public DetailprevisionsController(DBContext context)
        {
            _context = context;
        }

        // GET: Detailprevisions
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Detailprevisions.Include(d => d.IdprevisionNavigation).Include(d => d.IdquaiNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Detailprevisions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailprevision = await _context.Detailprevisions
                .Include(d => d.IdprevisionNavigation)
                .Include(d => d.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Iddetailprevision == id);
            if (detailprevision == null)
            {
                return NotFound();
            }

            return View(detailprevision);
        }

        // GET: Detailprevisions/Create
        public IActionResult Create()
        {
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale");
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai");
            return View();
        }

        // POST: Detailprevisions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Iddetailprevision,Idprevision,Idquai,Arrive,Depart")] Detailprevision detailprevision)
        {
            if (ModelState.IsValid)
            {
                _context.Add(detailprevision);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", detailprevision.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", detailprevision.Idquai);
            return View(detailprevision);
        }

        // GET: Detailprevisions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailprevision = await _context.Detailprevisions.FindAsync(id);
            if (detailprevision == null)
            {
                return NotFound();
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", detailprevision.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", detailprevision.Idquai);
            return View(detailprevision);
        }

        // POST: Detailprevisions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Iddetailprevision,Idprevision,Idquai,Arrive,Depart")] Detailprevision detailprevision)
        {
            if (id != detailprevision.Iddetailprevision)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(detailprevision);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DetailprevisionExists(detailprevision.Iddetailprevision))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idprevision"] = new SelectList(_context.Previsions, "Idprevision", "Noescale", detailprevision.Idprevision);
            ViewData["Idquai"] = new SelectList(_context.Quais, "Idquai", "Nomquai", detailprevision.Idquai);
            return View(detailprevision);
        }

        // GET: Detailprevisions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailprevision = await _context.Detailprevisions
                .Include(d => d.IdprevisionNavigation)
                .Include(d => d.IdquaiNavigation)
                .FirstOrDefaultAsync(m => m.Iddetailprevision == id);
            if (detailprevision == null)
            {
                return NotFound();
            }

            return View(detailprevision);
        }

        // POST: Detailprevisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var detailprevision = await _context.Detailprevisions.FindAsync(id);
            _context.Detailprevisions.Remove(detailprevision);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DetailprevisionExists(int id)
        {
            return _context.Detailprevisions.Any(e => e.Iddetailprevision == id);
        }
    }
}
