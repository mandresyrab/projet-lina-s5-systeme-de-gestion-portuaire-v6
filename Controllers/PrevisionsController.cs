﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Models;

namespace Projet_Lina_S5___Systeme_De_Gestion_Portuaire_v6.Controllers
{
    public class PrevisionsController : Controller
    {
        private readonly DBContext _context;

        public PrevisionsController(DBContext context)
        {
            _context = context;
        }

        // GET: Previsions
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Previsions.Include(p => p.IdnavireNavigation);
            return View(await dBContext.ToListAsync());
        }

        // GET: Previsions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prevision = await _context.Previsions
                .Include(p => p.IdnavireNavigation)
                .FirstOrDefaultAsync(m => m.Idprevision == id);
            if (prevision == null)
            {
                return NotFound();
            }

            return View(prevision);
        }

        // GET: Previsions/Create
        public IActionResult Create()
        {
            ViewData["Idnavire"] = new SelectList(_context.Navires, "Idnavire", "Nomnavire");
            return View();
        }

        // POST: Previsions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idprevision,Noescale,Idnavire")] Prevision prevision)
        {
            if (ModelState.IsValid)
            {
                _context.Add(prevision);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idnavire"] = new SelectList(_context.Navires, "Idnavire", "Nomnavire", prevision.Idnavire);
            return View(prevision);
        }

        // GET: Previsions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prevision = await _context.Previsions.FindAsync(id);
            if (prevision == null)
            {
                return NotFound();
            }
            ViewData["Idnavire"] = new SelectList(_context.Navires, "Idnavire", "Nomnavire", prevision.Idnavire);
            return View(prevision);
        }

        // POST: Previsions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idprevision,Noescale,Idnavire")] Prevision prevision)
        {
            if (id != prevision.Idprevision)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(prevision);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PrevisionExists(prevision.Idprevision))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idnavire"] = new SelectList(_context.Navires, "Idnavire", "Nomnavire", prevision.Idnavire);
            return View(prevision);
        }

        // GET: Previsions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prevision = await _context.Previsions
                .Include(p => p.IdnavireNavigation)
                .FirstOrDefaultAsync(m => m.Idprevision == id);
            if (prevision == null)
            {
                return NotFound();
            }

            return View(prevision);
        }

        // POST: Previsions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var prevision = await _context.Previsions.FindAsync(id);
            _context.Previsions.Remove(prevision);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PrevisionExists(int id)
        {
            return _context.Previsions.Any(e => e.Idprevision == id);
        }
    }
}
